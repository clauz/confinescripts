import json
import urllib2
import sys
import paramiko
import re
import socket
import threading
import networkx as nx

MAX_HOPS = 256
PING_TTL = 64

ttlpattern = re.compile("ttl=([0-9]*)")
rttpattern = re.compile("time=([0-9\.]*)")
iperfbwpattern = re.compile("\]\ +0.0-[0-9\.]* sec.* ([0-9\.]+) .bits/sec")
iperfprefixpattern = re.compile("\]\ +0.0-[0-9\.]* sec.* [0-9\.]+ (.)bits/sec")

class AutoAcceptPolicy(paramiko.MissingHostKeyPolicy):
    def missing_host_key(self, client, hostname, key):
        # accept everything
        return
    
class ConfineSliver(threading.Thread):
    def __init__(self, slivernumber=-1, slivergraph=None):
        threading.Thread.__init__(self)
        self.number = slivernumber
        self.public4 = None
        self.management = None
        self.connectivitydict = {}
        self.bandwidthdict = {}
        self.rttdict = {}
        if slivergraph == None:
            self.sliverGraph = nx.Graph()
        else:
            self.sliverGraph = slivergraph
        self.performPing = True
        self.performIperf = True
        self.performTraceroute = False
        # init the SSH connectivity
        self.shell = paramiko.SSHClient()
        self.shell.load_system_host_keys()
        self.shell.set_missing_host_key_policy(AutoAcceptPolicy())

    def storeHopsToDst(self, dst, hops):
        if isinstance(dst, ConfineSliver):
            k = dst.number
        else:
            k = dst
        self.connectivitydict[k] = hops

    def getHopsToDst(self, dst):
        if isinstance(dst, ConfineSliver):
            k = dst.number
        else:
            k = dst
        if self.connectivitydict.has_key(k):
            return self.connectivitydict[k]
        return MAX_HOPS

    def storeRtt(self, dst, rtt):
        if isinstance(dst, ConfineSliver):
            k = dst.number
        else:
            k = dst
        self.rttdict[k] = rtt

    def getRtt(self,dst):
        if isinstance(dst, ConfineSliver):
            k = dst.number
        else:
            k = dst
        if self.rttdict.has_key(k):
            return self.rttdict[k]
        return 0

    def pingAndStore(self, dst):
        if self.number == dst.number:
            self.storeHopsToDst(dst, 0)
            return
        sys.stderr.write("probing: %s --> %s\n" % (self.number, dst.number))
        try:
            self.shell.connect(self.management, username = "root")
            stdin, stdout, stderr = self.shell.exec_command("ping -c 3 -t %s %s" % (PING_TTL, dst.public4))
            output = stdout.read().strip()
            resttl = PING_TTL - int(re.findall(ttlpattern, output)[0])
            self.storeHopsToDst(dst, resttl)
            resrtt = float(re.findall(rttpattern, output)[-1])
            self.storeRtt(dst, resrtt)
            res = "%s --> %s : %s hops, %.2f RTT\n" % (self.number, dst.number, resttl, resrtt)
            sys.stdout.write(res)
            sys.stderr.write(res)
        except Exception as e:
            sys.stderr.write("remote ping command: %s\n" % str(e))
        self.shell.close()

    def getSliverGraph(self):
        return self.sliverGraph
    
    def tracerouteAndStore(self, dst):
        if self.number == dst.number:
            return
        sys.stderr.write("traceroute: %s --> %s (%s --> %s)\n" % (self.number, dst.number, self.public4, dst.public4))
        try:
            self.shell.connect(self.management, username = "root")
            stdin, stdout, stderr = self.shell.exec_command("traceroute -n -q 1 %s" % dst.public4)
            outputcopy = open("/tmp/slice.traceroute.%s.%s" % (self.number, dst.number), "w")
            hop0 = "%s" % self.public4
            hopname = self.management.split(':')[3]
            self.sliverGraph.add_node(hopname)
            #self.sliverGraph[hopname]['shape'] = "diamond"
            self.sliverGraph.add_edge(hopname, hop0)
            delay0 = 0.0
            while True:
                tracerouteline = stdout.readline() 
                outputcopy.write(tracerouteline)
                tracerouteline = tracerouteline.strip()
                if len(tracerouteline) <= 1:
                    break
                sys.stderr.write("%s\n" % tracerouteline)
                itemz = tracerouteline.split()
                if len(itemz) < 2 or itemz[1] == "*" or itemz[0] == "traceroute":
                    sys.stderr.write("jump\n")
                    continue
                hop1 = itemz[1]
                if hop0 == hop1:
                    sys.stderr.write("same node\n")
                    continue
                delay1 = float(itemz[2])
                delay = delay1 - delay0
                if delay < 0:
                    delay = 0.0
                self.sliverGraph.add_edge(hop0, hop1, label=delay)
                res = "%s --> %s:  %s -> %s %.2f ms\n" % (self.number, dst.number, hop0, hop1, delay)
                sys.stdout.write(res)
                sys.stderr.write(res)
                hop0 = hop1
                delay0 = delay1
            outputcopy.close()
            self.shell.close()
        except Exception as e:
            sys.stderr.write("%s: remote traceroute command: %s\n" % (self.number, str(e)))
            sys.stderr.write(stderr.read())
    
    def storeBwToDst(self, dst, bw):
        if isinstance(dst, ConfineSliver):
            k = dst.number
        else:
            k = dst
        self.bandwidthdict[k] = bw
    
    def getBwToDst(self, dst):
        if isinstance(dst, ConfineSliver):
            k = dst.number
        else:
            k = dst
        if self.bandwidthdict.has_key(k):
            return self.bandwidthdict[k] 
        return -1

    def iperfServer(self):
        sys.stderr.write("iperf -s: %s\n" % self.number)
        try:
            self.shell.connect(self.management, username = "root")
            cmd = "screen -d -m -S iperfs iperf -s -i 3 -f k -p 5003" 
            sys.stderr.write("%s\n" % cmd)
            stdin, stdout, stderr = self.shell.exec_command(cmd)
        except Exception as e:
            sys.stderr.write("remote iperf -s command: %s\n" % str(e))

    def iperfAndStore(self, dst):
        if self.number == dst.number:
            self.storeBwToDst(dst, -1)
            return
        sys.stderr.write("iperf: %s --> %s\n" % (self.number, dst.number))
        sys.stderr.write("iperf: %s --> %s\n" % (self.nodeinfo['name'], dst.nodeinfo['name']))
        try:
            self.shell.connect(self.management, username = "root")
            cmd = "iperf -c %s -t 60 -i 3 -f k -p 5003" % dst.public4
            sys.stderr.write("%s\n" % cmd)
            stdin, stdout, stderr = self.shell.exec_command(cmd)
            output = stdout.read().strip()
            sys.stderr.write("o-------\n%s\n" % output)
            sys.stderr.write("e-------\n" + stderr.read() + "\n")
            resbw = re.findall(iperfbwpattern, output)[0]
            sys.stderr.write("r-------" + resbw + "\n")
            self.storeBwToDst(dst, float(resbw))
            res = "%s --> %s : %s\n" % (self.number, dst.number, resbw)
            sys.stdout.write(res)
            sys.stderr.write(res)
        except Exception as e:
            sys.stderr.write("remote iperf command: %s\n" % str(e))
        self.shell.close()

    def setDestinations(self, destinations):
        "the destination slivers to ping"
        self.destinations = destinations

    def run(self):
        if self.performIperf:
            self.iperfServer()
        if self.performPing:
            for dst in self.destinations:
                self.pingAndStore(dst)
        if self.performIperf:
            for dst in self.destinations:
                self.iperfAndStore(dst)
        if self.performTraceroute:
            for dst in self.destinations:
                self.tracerouteAndStore(dst)

def fetchnparse(uri):
    # try 3 times
    f = None
    attempts = 0
    while f == None and attempts < 3:
        try:
            f = urllib2.urlopen(uri)
        except Exception,e:
            sys.stderr.write(str(Exception) + " " + str(e))
            f = None
            attempts += 1
    if f == None:
        return {'state': 'unavailable'}
    res = f.read()
    f.close()
    try:
        res = res.decode('ascii', 'ignore')
    except:
        print res
        raise
    jres = json.loads(res)
    return jres

def simplifyGraph(G):
    "remove all nodes with degree == 2"
    initialdegree2nodes = [n for n in G.nodes() if G.degree(n) == 2]
    for a in initialdegree2nodes:
        nlist = [n for n in G.neighbors(a)]
        print "neighbors for %s : %s" % (a, nlist)
        if len(nlist) < 2:
            print "skipping!"
            continue
        b = nlist[0]
        c = nlist[1]
        try:
            delayab = float(G[a][b]['label'])
            delayac = float(G[a][c]['label'])
            delaysum = "%.2f" % (delayab + delayac)
        except:
            delaysum = ""
        G.remove_node(a)
        print "removed node %s" % a
        if(len(delaysum) > 0):
            G.add_edge(b, c, label=delaysum)
        else:
            G.add_edge(b, c)
        print "added   edge %s <- %s -> %s" % (b, delaysum, c)

