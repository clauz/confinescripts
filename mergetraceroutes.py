#!/usr/bin/env python2

import sys

if len(sys.argv) < 2:
    print "Usage: %s <big file with a concatenation of all traceroutes>" % sys.argv[0]
    sys.exit(1)

import networkx as nx
from common import simplifyGraph

G = nx.Graph()

infile = open(sys.argv[1])
hop0 = None
delay0 = 0.0
for line in infile:
    tracerouteline = line.strip()
    if len(tracerouteline) <= 1:
        hop0 = None
        delay0 = 0.0
        continue
    itemz = tracerouteline.split()
    if len(itemz) < 2 or itemz[1] == "*" or itemz[0] == "traceroute" or itemz[1] == "traceroute":
        hop0 = None
        delay0 = 0.0
        continue
    print itemz
    hop1 = itemz[1]
    delay1 = float(itemz[2])
    if hop0 != None:
        delay1 = float(itemz[2])
        delay = delay1 - delay0
        #G.add_edge(hop0, hop1, weight=delay)
        G.add_edge(hop0, hop1)
    hop0 = hop1
    delay0 = delay1

infile.close()
nx.write_dot(G, 'out_complete.dot')

# remove all nodes with degree == 2
simplifyGraph(G)
nx.write_dot(G, 'out_simplified.dot')


