#!/bin/bash

PROTO=udp
BINPATH=/root/bin
NAME="erdetep_3nodesguifidaje_$(date +'%s')"
CCNRESOURCE=20M

declare -A mgmt_addresses
mgmt_addresses[n0]="fdf5:5351:1dfd:008c:1002:0000:0000:00a5"
mgmt_addresses[r1]="fdf5:5351:1dfd:00c6:1002:0000:0000:00a5"
mgmt_addresses[r2]="fdf5:5351:1dfd:00ae:1002:0000:0000:00a5"

declare -A pub_addresses
pub_addresses[n0]="10.1.24.124"     #8c
pub_addresses[r1]="10.228.203.220"  #c6
pub_addresses[r2]="10.228.44.48"    #ae

#pub_addresses[n0]="10.228.17.52"    #97 #packet loss
#pub_addresses[r1]="10.1.67.24"      #b3
#pub_addresses[n1]="10.1.32.41"      #c5
#pub_addresses[n0]="10.38.141.27"    #5a : 76, 88, 181
#pub_addresses[n0]="10.228.192.156"  #11 :123, 146, 202
#pub_addresses[r1]="10.228.206.81"   #5b
#pub_addresses[n0]="10.138.42.139"   #70
#pub_addresses[r1]="10.1.67.24"      #b3
#pub_addresses[r1]="10.1.32.41"      #c5
#pub_addresses[r2]="10.38.141.27"    #5a
#pub_addresses[r3]="10.138.42.139"   #70
#pub_addresses[r3]="10.1.67.24"      #b3

ossh() {
    echo "$@"
    ssh -q -o "UserKnownHostsFile /dev/null" -o "StrictHostKeyChecking no" -l root "$@"
}

oscp() {
    scp -q -o "UserKnownHostsFile /dev/null" -o "StrictHostKeyChecking no" -C "$@"
}

gatherfile() {
    FILENAME="$1"
    TEST="test_${2}"
    TESTDIR="${NAME}/${TEST}"
    TARGET="$3"
    echo "$TESTDIR"
    mkdir -p "$TESTDIR"
    oscp root@[${mgmt_addresses[${TARGET}]}]:/tmp/${FILENAME} ${TESTDIR}/
}

gatherg() {
    gatherfile ccngetfile.log $@
}

gatherg2() {
    gatherfile ccngetfile2.log $@
}

gathern() {
    TARGET="$2"
    gatherfile ccnd.${TARGET}.log $@
}

gatherr() {
    TEST="test_${1}"
    TESTDIR="${NAME}/${TEST}"
    TARGET="$2"
    mkdir -p "$TESTDIR"
    oscp root@[${mgmt_addresses[${TARGET}]}]:/tmp/bitrate.log ${TESTDIR}/bitrate.${TARGET}.log
}

addccndefault() {
    src="$1"
    dst="$2"
    strategy=${3:-1}
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndstop"
    ossh ${mgmt_addresses[$src]} "CCND_LOG=/tmp/ccnd.${1}.log CCN_STRATEGY=${strategy} nohup ${BINPATH}/ccndstart > /dev/null < /dev/null"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndc add ccnx:/          $PROTO ${pub_addresses[$dst]}"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndstatus"
}

addccnAandBroutes() {
    src="$1"
    dst1="$2"
    dst2="$3"
    strategy=${4:-1}
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndstop"
    ossh ${mgmt_addresses[$src]} "CCND_LOG=/tmp/ccnd.${src}.log CCN_STRATEGY=${strategy} nohup ${BINPATH}/ccndstart > /dev/null < /dev/null"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndc add ccnx:/ccnx.org/ $PROTO ${pub_addresses[$dst1]}"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndc add ccnx:/ccnx.org/ $PROTO ${pub_addresses[$dst2]}"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndc add ccnx:/AandB/    $PROTO ${pub_addresses[$dst1]}"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndc add ccnx:/AandB/    $PROTO ${pub_addresses[$dst2]}"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndstatus"
}

# SINGLE PATH 1
# n0 -> r1
ossh ${mgmt_addresses[r1]} "echo '' > /tmp/bitrate.log"

addccndefault n0 r1

ossh ${mgmt_addresses[n0]} "JAVA_PIPELINE_RTTFACTOR=8 JAVA_PIPELINE_STATS=true ${BINPATH}/ccngetfile -v -unversioned ccnx:/AandB/${CCNRESOURCE} /dev/null > /tmp/ccngetfile.log"

ossh ${mgmt_addresses[n0]} "${BINPATH}/ccndstop"

gatherg n0r1 n0
gathern n0r1 n0
gatherr n0r1 r1

# SINGLE PATH 2
# n0 -> r2
ossh ${mgmt_addresses[r2]} "echo '' > /tmp/bitrate.log"

addccndefault n0 r2

ossh ${mgmt_addresses[n0]} "JAVA_PIPELINE_RTTFACTOR=8 JAVA_PIPELINE_STATS=true ${BINPATH}/ccngetfile -v -unversioned ccnx:/AandB/${CCNRESOURCE} /dev/null > /tmp/ccngetfile.log"

ossh ${mgmt_addresses[n0]} "${BINPATH}/ccndstop"

gatherg n0n1r2 n0
gathern n0n1r2 n0
gatherr n0n1r2 r2

# MULTIPATH ERDETEP
ossh ${mgmt_addresses[r1]} "echo '' > /tmp/bitrate.log"
ossh ${mgmt_addresses[r2]} "echo '' > /tmp/bitrate.log"

addccnAandBroutes n0 r1 r2 1

ossh ${mgmt_addresses[n0]} "JAVA_PIPELINE_RTTFACTOR=8 JAVA_PIPELINE_STATS=true ${BINPATH}/ccngetfile -v -unversioned ccnx:/AandB/${CCNRESOURCE} /dev/null > /tmp/ccngetfile.log"

ossh ${mgmt_addresses[n0]} "${BINPATH}/ccndstop"

gatherg multipatherdetep n0
gathern multipatherdetep n0
gatherr multipatherdetep r1
gatherr multipatherdetep r2

# MULTIPATH RTT_EQ
ossh ${mgmt_addresses[r1]} "echo '' > /tmp/bitrate.log"
ossh ${mgmt_addresses[r2]} "echo '' > /tmp/bitrate.log"

addccnAandBroutes n0 r1 r2 2

ossh ${mgmt_addresses[n0]} "JAVA_PIPELINE_RTTFACTOR=8 JAVA_PIPELINE_STATS=true ${BINPATH}/ccngetfile -v -unversioned ccnx:/AandB/${CCNRESOURCE} /dev/null > /tmp/ccngetfile.log"

ossh ${mgmt_addresses[n0]} "${BINPATH}/ccndstop"

gatherg multipathrtteq n0
gathern multipathrtteq n0
gatherr multipathrtteq r1
gatherr multipathrtteq r2

# MULTIPATH PIPELINE_EQ
ossh ${mgmt_addresses[r1]} "echo '' > /tmp/bitrate.log"
ossh ${mgmt_addresses[r2]} "echo '' > /tmp/bitrate.log"

addccnAandBroutes n0 r1 r2 3

ossh ${mgmt_addresses[n0]} "JAVA_PIPELINE_RTTFACTOR=8 JAVA_PIPELINE_STATS=true ${BINPATH}/ccngetfile -v -unversioned ccnx:/AandB/${CCNRESOURCE} /dev/null > /tmp/ccngetfile.log"

ossh ${mgmt_addresses[n0]} "${BINPATH}/ccndstop"

gatherg multipathpipeeq n0
gathern multipathpipeeq n0
gatherr multipathpipeeq r1
gatherr multipathpipeeq r2


tail -n 2 ${NAME}/*/ccngetfile*.log

