#!/bin/bash

PROTO=udp
BINPATH=/root/bin
NAME="erdetep_5nodes_$(date +'%s')"

declare -A mgmt_addresses
mgmt_addresses[n0]="fdf5:5351:1dfd:008c:1002:0000:0000:00a5"
mgmt_addresses[n1]="fdf5:5351:1dfd:00c6:1002:0000:0000:00a5"
mgmt_addresses[r1]="fdf5:5351:1dfd:00c5:1002:0000:0000:00a5"
#mgmt_addresses[r2]="fdf5:5351:1dfd:0011:1002:0000:0000:00a5"
mgmt_addresses[r2]="fdf5:5351:1dfd:00b3:1002:0000:0000:00a5"
mgmt_addresses[r3]="fdf5:5351:1dfd:005b:1002:0000:0000:00a5"

declare -A pub_addresses
pub_addresses[n0]="10.1.24.124"     #8c
pub_addresses[n1]="10.228.203.220"  #c6
pub_addresses[r1]="10.1.32.41"      #c5
#pub_addresses[r2]="10.228.192.156"  #11
pub_addresses[r2]="10.1.67.24"      #b3
pub_addresses[r3]="10.228.206.74"   #5b

ossh() {
    echo "$@"
    ssh -q -o "UserKnownHostsFile /dev/null" -o "StrictHostKeyChecking no" -l root "$@"
}

oscp() {
    scp -q -o "UserKnownHostsFile /dev/null" -o "StrictHostKeyChecking no" -C "$@"
}

gatherg() {
    TEST="test_${1}"
    TESTDIR="${NAME}/${TEST}"
    TARGET="$2"
    echo "$TESTDIR"
    mkdir -p "$TESTDIR"
    oscp root@[${mgmt_addresses[${TARGET}]}]:/tmp/ccngetfile*.log ${TESTDIR}/
    tail -n 2 ${TESTDIR}/ccngetfile*.log
}

gathern() {
    TEST="test_${1}"
    TESTDIR="${NAME}/${TEST}"
    TARGET="$2"
    mkdir -p "$TESTDIR"
    oscp root@[${mgmt_addresses[${TARGET}]}]:/tmp/ccnd.${TARGET}.log ${TESTDIR}/
}

gatherr() {
    TEST="test_${1}"
    TESTDIR="${NAME}/${TEST}"
    TARGET="$2"
    mkdir -p "$TESTDIR"
    oscp root@[${mgmt_addresses[${TARGET}]}]:/tmp/bitrate.log ${TESTDIR}/bitrate.${TARGET}.log
}

addccndefault() {
    src="$1"
    dst="$2"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndstop"
    ossh ${mgmt_addresses[$src]} "CCND_LOG=/tmp/ccnd.n1.log nohup ${BINPATH}/ccndstart > /dev/null < /dev/null"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndc add ccnx:/          $PROTO ${pub_addresses[$dst]}"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndstatus"
}

addccnAandBroutes() {
    src="$1"
    dst1="$2"
    dst2="$3"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndstop"
    ossh ${mgmt_addresses[$src]} "CCND_LOG=/tmp/ccnd.${src}.log nohup ${BINPATH}/ccndstart > /dev/null < /dev/null"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndc add ccnx:/ccnx.org/ $PROTO ${pub_addresses[$dst1]}"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndc add ccnx:/ccnx.org/ $PROTO ${pub_addresses[$dst2]}"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndc add ccnx:/AandB/    $PROTO ${pub_addresses[$dst1]}"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndc add ccnx:/AandB/    $PROTO ${pub_addresses[$dst2]}"
    ossh ${mgmt_addresses[$src]} "${BINPATH}/ccndstatus"
}

killall ssh
sleep 5

# MULTIPATH
ossh ${mgmt_addresses[r1]} "echo '' > /tmp/bitrate.log"
ossh ${mgmt_addresses[r2]} "echo '' > /tmp/bitrate.log"
ossh ${mgmt_addresses[r3]} "echo '' > /tmp/bitrate.log"

addccnAandBroutes n1 r1 r2
addccnAandBroutes n0 n1 r3

ossh ${mgmt_addresses[n0]} "JAVA_PIPELINE_STATS=true ${BINPATH}/ccngetfile -v -unversioned ccnx:/AandB/100M /dev/null > /tmp/ccngetfile.log"

ossh ${mgmt_addresses[n0]} "${BINPATH}/ccndstop"
ossh ${mgmt_addresses[n1]} "${BINPATH}/ccndstop"

gatherg multipath n0
gathern multipath n0
gathern multipath n1
gatherr multipath r1
gatherr multipath r2
gatherr multipath r3

# SINGLE PATH 1
# n0 -> n1 -> r1
ossh ${mgmt_addresses[r1]} "echo '' > /tmp/bitrate.log"

addccndefault n1 r1
addccndefault n0 n1

ossh ${mgmt_addresses[n0]} "JAVA_PIPELINE_STATS=true ${BINPATH}/ccngetfile -v -unversioned ccnx:/AandB/100M /dev/null > /tmp/ccngetfile.log"

ossh ${mgmt_addresses[n0]} "${BINPATH}/ccndstop"
ossh ${mgmt_addresses[n1]} "${BINPATH}/ccndstop"

gatherg n0n1r1 n0
gathern n0n1r1 n0
gathern n0n1r1 n1
gatherr n0n1r1 r1

# SINGLE PATH 2
# n0 -> n1 -> r2
ossh ${mgmt_addresses[r2]} "echo '' > /tmp/bitrate.log"

addccndefault n1 r2
addccndefault n0 n1

ossh ${mgmt_addresses[n0]} "JAVA_PIPELINE_STATS=true ${BINPATH}/ccngetfile -v -unversioned ccnx:/AandB/100M /dev/null > /tmp/ccngetfile.log"

ossh ${mgmt_addresses[n0]} "${BINPATH}/ccndstop"
ossh ${mgmt_addresses[n1]} "${BINPATH}/ccndstop"

gatherg n0n1r2 n0
gathern n0n1r2 n0
gathern n0n1r2 n1
gatherr n0n1r2 r1

# SINGLE PATH 3
# n0 -> r3
ossh ${mgmt_addresses[r3]} "echo '' > /tmp/bitrate.log"

addccndefault n0 r3

ossh ${mgmt_addresses[n0]} "JAVA_PIPELINE_STATS=true ${BINPATH}/ccngetfile -v -unversioned ccnx:/AandB/100M /dev/null > /tmp/ccngetfile.log"

ossh ${mgmt_addresses[n0]} "${BINPATH}/ccndstop"

gatherg n0r3 n0
gathern n0r3 n0
gatherr n0r3 r3

# MULTIPATH AND PARALLEL SINGLE

ossh ${mgmt_addresses[r1]} "echo '' > /tmp/bitrate.log"
ossh ${mgmt_addresses[r2]} "echo '' > /tmp/bitrate.log"
ossh ${mgmt_addresses[r3]} "echo '' > /tmp/bitrate.log"

addccnAandBroutes n1 r1 r2
addccnAandBroutes n0 n1 r3

ossh ${mgmt_addresses[n1]} "${BINPATH}/ccndc add ccnx:/onlyA/    $PROTO ${pub_addresses[r1]}"
ossh ${mgmt_addresses[n0]} "${BINPATH}/ccndc add ccnx:/onlyA/    $PROTO ${pub_addresses[n1]}"
ossh ${mgmt_addresses[n1]} "${BINPATH}/ccndstatus"
ossh ${mgmt_addresses[n0]} "${BINPATH}/ccndstatus"

ossh ${mgmt_addresses[n0]} "JAVA_PIPELINE_STATS=true ${BINPATH}/ccngetfile -v -unversioned ccnx:/onlyA/100M /dev/null > /tmp/ccngetfile.log" &
ossh ${mgmt_addresses[n0]} "JAVA_PIPELINE_STATS=true ${BINPATH}/ccngetfile -v -unversioned ccnx:/AandB/100M /dev/null > /tmp/ccngetfile2.log"
wait

ossh ${mgmt_addresses[n0]} "${BINPATH}/ccndstop"
ossh ${mgmt_addresses[n1]} "${BINPATH}/ccndstop"

gatherg multisingle n0
gathern multisingle n0
gathern multisingle n1
gatherr multisingle r1
gatherr multisingle r2
gatherr multisingle r3

