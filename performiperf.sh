#!/bin/bash

declare -A mgmt_addresses
mgmt_addresses[941]="fdf5:5351:1dfd:0097:1002:0000:0000:006d"
mgmt_addresses[942]="fdf5:5351:1dfd:008c:1002:0000:0000:006d"
mgmt_addresses[943]="fdf5:5351:1dfd:008b:1002:0000:0000:006d"
mgmt_addresses[944]="fdf5:5351:1dfd:0083:1002:0000:0000:006d"
mgmt_addresses[945]="fdf5:5351:1dfd:0081:1002:0000:0000:006d"
mgmt_addresses[947]="fdf5:5351:1dfd:007c:1002:0000:0000:006d"
mgmt_addresses[948]="fdf5:5351:1dfd:0078:1002:0000:0000:006d"
mgmt_addresses[951]="fdf5:5351:1dfd:006f:1002:0000:0000:006d"
mgmt_addresses[952]="fdf5:5351:1dfd:0064:1002:0000:0000:006d"
mgmt_addresses[953]="fdf5:5351:1dfd:0063:1002:0000:0000:006d"
mgmt_addresses[954]="fdf5:5351:1dfd:0062:1002:0000:0000:006d"
mgmt_addresses[956]="fdf5:5351:1dfd:005a:1002:0000:0000:006d"
mgmt_addresses[957]="fdf5:5351:1dfd:0011:1002:0000:0000:006d"
mgmt_addresses[960]="fdf5:5351:1dfd:0003:1002:0000:0000:006d"
mgmt_addresses[961]="fdf5:5351:1dfd:0002:1002:0000:0000:006d"
mgmt_addresses[962]="fdf5:5351:1dfd:0001:1002:0000:0000:006d"

declare -A pub_addresses
pub_addresses[941]="10.228.17.54"
pub_addresses[942]="10.1.24.122"
pub_addresses[943]="10.228.28.26"
pub_addresses[944]="192.168.10.21"
pub_addresses[945]="10.228.21.66"
pub_addresses[947]="10.228.16.150"
pub_addresses[948]="10.228.33.4"
pub_addresses[951]="10.138.57.204"
pub_addresses[952]="192.168.1.155"
pub_addresses[953]="192.168.1.163"
pub_addresses[954]="10.228.21.150"
pub_addresses[956]="10.38.141.29"
pub_addresses[957]="10.228.192.156"
pub_addresses[960]="10.228.20.230"
pub_addresses[961]="10.228.21.237"
pub_addresses[962]="10.228.18.40"

ossh() {
    ssh -o "UserKnownHostsFile /dev/null" -o "StrictHostKeyChecking no" -l root $@
}

for m in ${mgmt_addresses[@]}; do
    echo "----> start iperf server on $m"
    ossh $m "iperf -s -i 3 -p 5005" > /dev/null 2>&1 &
done

sleep 5

for s in ${!mgmt_addresses[@]}; do
    for t in ${!pub_addresses[@]}; do
        [ "$s" == "$t" ] && continue
        src=${mgmt_addresses[$s]}
        dst=${pub_addresses[$t]}
        echo "-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_"
        echo "> $s --> $t ; $src --> $dst"
        dstfile="iperf_${s}_${t}"
        ossh $src "iperf -c $dst -i 3 -t 60 -p 5005" > $dstfile 2>&1
        tail $dstfile
        echo "-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_"
    done
done

