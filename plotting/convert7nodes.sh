#!/bin/bash

CCN_LOGFILE0=ccnd.n0.log
CCN_LOGFILE1=ccnd.n1.log
CCN_LOGFILE2=ccnd.n2.log
CCNGETFILE_LOGFILE=ccngetfile.log
BITRATE_LOGFILE1=bitrate.r1.log
BITRATE_LOGFILE2=bitrate.r2.log
BITRATE_LOGFILE3=bitrate.r3.log
BITRATE_LOGFILE4=bitrate.r4.log

PIPELINE_THRESHOLD_MAX=10000

#1391608550.948796 ccnd[21988]: Display ccnx:/AandB/100M, face: 7, timeout: 83.98 ms, pipeline_threshold: 10000, pending_interests_counter: 1, choice_delta: 10000.315 max: 11, rtt: 2, low_th: 11, wtnow: 8589816, threshold_set_time: 0, difference: 67260, reset: 0
# 1  -> timestamp               -> 1
# 4  -> prefix
# 6  -> face
# 8  -> timeout
# 11 -> threshold               -> 2
# 13 -> pending_interests       -> 3
# 15 -> choice_delta            -> 4
# 17 -> max_pending_interests   -> 5
# 19 -> rtt                     -> 6
# 21 -> low_threshold           -> 7
# 23 -> wtnow
# 25 -> threshold_set_time
# 27 -> difference
# 29 -> reset
# 31 -> sent_interests_counter  -> 8
# 33 -> recv_interests_counter  -> 9
# 34 -> timeout_interests_counter  -> 10

cat $CCN_LOGFILE0 | grep Display | grep "face: 7" | sed 's/,//g' | sed "s/pipeline_threshold: ${PIPELINE_THRESHOLD_MAX}/pipeline_threshold: 0/g" | grep "ccnx:/AandB/100M "  | awk '{print $1 " " $11 " " $13 " " $15 " " $17 " " $19 " " $21 " " $31 " " $33 " " $34}' > n0.face7.AandB100M.up
cat $CCN_LOGFILE0 | grep Display | grep "face: 9" | sed 's/,//g' | sed "s/pipeline_threshold: ${PIPELINE_THRESHOLD_MAX}/pipeline_threshold: 0/g" | grep "ccnx:/AandB/100M "  | awk '{print $1 " " $11 " " $13 " " $15 " " $17 " " $19 " " $21 " " $31 " " $33 " " $34}' > n0.face9.AandB100M.up

cat $CCN_LOGFILE1 | grep Display | grep "face: 7" | sed 's/,//g' | sed "s/pipeline_threshold: ${PIPELINE_THRESHOLD_MAX}/pipeline_threshold: 0/g" | grep "ccnx:/AandB/100M "  | awk '{print $1 " " $11 " " $13 " " $15 " " $17 " " $19 " " $21 " " $31 " " $33 " " $34}' > n1.face7.AandB100M.up
cat $CCN_LOGFILE1 | grep Display | grep "face: 9" | sed 's/,//g' | sed "s/pipeline_threshold: ${PIPELINE_THRESHOLD_MAX}/pipeline_threshold: 0/g" | grep "ccnx:/AandB/100M "  | awk '{print $1 " " $11 " " $13 " " $15 " " $17 " " $19 " " $21 " " $31 " " $33 " " $34}' > n1.face9.AandB100M.up

cat $CCN_LOGFILE2 | grep Display | grep "face: 7" | sed 's/,//g' | sed "s/pipeline_threshold: ${PIPELINE_THRESHOLD_MAX}/pipeline_threshold: 0/g" | grep "ccnx:/AandB/100M "  | awk '{print $1 " " $11 " " $13 " " $15 " " $17 " " $19 " " $21 " " $31 " " $33 " " $34}' > n2.face7.AandB100M.up
cat $CCN_LOGFILE2 | grep Display | grep "face: 9" | sed 's/,//g' | sed "s/pipeline_threshold: ${PIPELINE_THRESHOLD_MAX}/pipeline_threshold: 0/g" | grep "ccnx:/AandB/100M "  | awk '{print $1 " " $11 " " $13 " " $15 " " $17 " " $19 " " $21 " " $31 " " $33 " " $34}' > n2.face9.AandB100M.up

cat $CCNGETFILE_LOGFILE | grep plot | grep -v "plot 0" | awk '{print $2 " " $17 " " $12 " " $8 " " $4 " " $6 " " $19}' > ccngetfile.win

cat $CCN_LOGFILE0 | grep dup | awk '{print $1}' > n0.dups
cat $CCN_LOGFILE0 | grep "SET PIPELINE THRESHOLD" | grep "face 7" | awk '{print $1 " " $7 " " $14}' > n0.thresholds.face7
cat $CCN_LOGFILE0 | grep "SET PIPELINE THRESHOLD" | grep "face 9" | awk '{print $1 " " $7 " " $14}' > n0.thresholds.face9
cat $CCN_LOGFILE0 | grep LOSS | grep "face 7" | awk '{print $1}' > n0.losses.face7
cat $CCN_LOGFILE0 | grep LOSS | grep "face 9" | awk '{print $1}' > n0.losses.face9
cat $CCN_LOGFILE0 | grep LIFETIME | grep "forie" | awk '{print $1 " " $4}' > n0.lifetimes.forie
cat $CCN_LOGFILE0 | grep "interest satisfied" | awk '{print $1}' > n0.satisfied

cat $CCN_LOGFILE1 | grep dup | awk '{print $1}' > n1.dups
cat $CCN_LOGFILE1 | grep "SET PIPELINE THRESHOLD" | grep "face 7" | awk '{print $1 " " $7 " " $14}' > n1.thresholds.face7
cat $CCN_LOGFILE1 | grep "SET PIPELINE THRESHOLD" | grep "face 9" | awk '{print $1 " " $7 " " $14}' > n1.thresholds.face9
cat $CCN_LOGFILE1 | grep LOSS | grep "face 7" | awk '{print $1}' > n1.losses.face7
cat $CCN_LOGFILE1 | grep LOSS | grep "face 9" | awk '{print $1}' > n1.losses.face9
cat $CCN_LOGFILE1 | grep LIFETIME | grep "forie" | awk '{print $1 " " $4}' > n1.lifetimes.forie
cat $CCN_LOGFILE1 | grep "interest satisfied" | awk '{print $1}' > n1.satisfied

cat $CCN_LOGFILE2 | grep dup | awk '{print $1}' > n2.dups
cat $CCN_LOGFILE2 | grep "SET PIPELINE THRESHOLD" | grep "face 7" | awk '{print $1 " " $7 " " $14}' > n2.thresholds.face7
cat $CCN_LOGFILE2 | grep "SET PIPELINE THRESHOLD" | grep "face 9" | awk '{print $1 " " $7 " " $14}' > n2.thresholds.face9
cat $CCN_LOGFILE2 | grep LOSS | grep "face 7" | awk '{print $1}' > n2.losses.face7
cat $CCN_LOGFILE2 | grep LOSS | grep "face 9" | awk '{print $1}' > n2.losses.face9
cat $CCN_LOGFILE2 | grep LIFETIME | grep "forie" | awk '{print $1 " " $4}' > n2.lifetimes.forie
cat $CCN_LOGFILE2 | grep "interest satisfied" | awk '{print $1}' > n2.satisfied


