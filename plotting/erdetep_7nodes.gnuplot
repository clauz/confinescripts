#!/usr/bin/gnuplot

set terminal png notransparent truecolor large enhanced font DejaVuSansMono 10 size 1024,768;

set output 'out.png';
set ylabel 'values';
set xtics 5
set grid xtics ytics mytics;
set multiplot layout 5,1 columnsfirst;

timemin=`head -n 1 ccngetfile.win | awk '{print $1}'`
#tcqueuefactor=4.0

set tmargin 1
set bmargin 2
set lmargin 5
set rmargin 1

set xrange [0:50] 

plot \
 'ccngetfile.win' using ($1-timemin):2 title 'ccngetfile window 1' with lines linewidth 2,\
 'dups' using ($1-timemin):(1.0) title 'dups' with points linewidth 2;

set xrange [GPVAL_X_MIN:GPVAL_X_MAX] 
#set yrange [GPVAL_Y_MIN:GPVAL_Y_MAX * 4/5] 

plot \
 'ccngetfile.win' using ($1-timemin):2 title 'ccngetfile window 1' with lines linewidth 2,\
 'n0.face7.AandB100M.up' using ($1-timemin):3 title 'AandB/100M face7 pending interests' with lines linewidth 2,\
 'n0.face7.AandB100M.up' using ($1-timemin):5 title 'AandB/100M face7 max pending' with lines linewidth 2,\
 'n0.face7.AandB100M.up' using ($1-timemin):7 title 'AandB/100M face7 low threshold' with lines linewidth 2,\
 'n0.thresholds.face7' using ($1-timemin):2:($1-timemin):($1-timemin+$3) title 'face9 threshold' with xerrorbars linewidth 2,\
 'n0.losses.face7' using ($1-timemin):(5.0) title 'face7 losses' with points linewidth 2;

plot \
 'ccngetfile.win' using ($1-timemin):2 title 'ccngetfile window 1' with lines linewidth 2,\
 'n0.face9.AandB100M.up' using ($1-timemin):3 title 'AandB/100M face9 pending interests' with lines linewidth 2,\
 'n0.face9.AandB100M.up' using ($1-timemin):5 title 'AandB/100M face9 max pending' with lines linewidth 2,\
 'n0.face9.AandB100M.up' using ($1-timemin):7 title 'AandB/100M face9 low threshold' with lines linewidth 2,\
 'n0.thresholds.face9' using ($1-timemin):2:($1-timemin):($1-timemin+$3) title 'face9 threshold' with xerrorbars linewidth 2,\
 'n0.losses.face9' using ($1-timemin):(5.0) title 'face9 losses' with points linewidth 2;

plot \
 'n0.face7.AandB100M.up' using ($1-timemin):6 title 'n0 AandB/100M face7 rtt' with lines linewidth 2,\
 'n0.face9.AandB100M.up' using ($1-timemin):6 title 'n0 AandB/100M face9 rtt' with lines linewidth 2,\
 'n1.face7.AandB100M.up' using ($1-timemin):6 title 'n1 AandB/100M face7 rtt' with lines linewidth 2,\
 'n1.face9.AandB100M.up' using ($1-timemin):6 title 'n1 AandB/100M face9 rtt' with lines linewidth 2,\
 'n2.face7.AandB100M.up' using ($1-timemin):6 title 'n2 AandB/100M face7 rtt' with lines linewidth 2,\
 'n2.face9.AandB100M.up' using ($1-timemin):6 title 'n2 AandB/100M face9 rtt' with lines linewidth 2;

plot \
 'bitrate.r1.log' using ($1-timemin):3 title 'r1 bandwidth' with lines linewidth 2,\
 'bitrate.r2.log' using ($1-timemin):3 title 'r2 bandwidth' with lines linewidth 2,\
 'bitrate.r3.log' using ($1-timemin):3 title 'r3 bandwidth' with lines linewidth 2,\
 'bitrate.r4.log' using ($1-timemin):3 title 'r4 bandwidth' with lines linewidth 2;

unset multiplot

#plot \
# 'n1.face7.AandB100M.up' using ($1-timemin):8 title 'AandB/100M face7 sent' with lines linewidth 2,\
# 'n1.face7.AandB100M.up' using ($1-timemin):(-$9) title 'AandB/100M face7 satisfied' with lines linewidth 2,\
# 'n1.face7.AandB100M.up' using ($1-timemin):(-$10) title 'AandB/100M face7 timed out' with lines linewidth 2;

 #'n1.face7.AandB100M.up' using ($1-timemin):($8-$9) title 'AandB/100M face7 sent-recv-diff' with lines linewidth 2,\
 #'n1.face7.AandB100M.up' using ($1-timemin):9 title 'AandB/100M face7 received' with lines linewidth 2,\
 #'n1.face7.AandB100M.up' using ($1-timemin):($8 * -1) title 'AandB/100M face7 sent' with lines linewidth 2,\
 #'socketqueue' using ($1-timemin):($2 / 5000.0) title 'socket queue' with lines linewidth 2,\
 #'satisfied' using ($1-timemin):(10.0) title 'interest satisfied' with points linewidth 2,\
#plot \
# 'ccngetfile.win' using ($1-timemin):($7/-1000.0) title 'ccngetfile timeouts' with points linewidth 1,\
# 'lifetimes.forie' using ($1-timemin+shift):2 title 'interest lifetimes 4ie' with points linewidth 1;

#set title 'face 9'

#set autoscale y;
##set yrange [0:300] 
#
#plot \
# 'n1.face7.AandB100M.up' using ($1-timemin):6 title 'AandB/100M face7 rtt' with lines linewidth 2;

#plot \
 #'ccngetfile.win' using ($1-timemin):4 title 'ccngetfile interests' with lines linewidth 2,\
 #'ccngetfile.win' using ($1-timemin):6 title 'ccngetfile outOfOrder' with lines linewidth 2,\
 #'ccngetfile.win' using ($1-timemin):5 title 'ccngetfile inOrder' with lines linewidth 2,\
 #'ccngetfile.win' using ($1-timemin):3 title 'ccngetfile received' with lines linewidth 2,\
 #'ccngetfile.win' using ($1-timemin):4 title 'ccngetfile interests' with lines linewidth 2,\
 #'ccngetfile.win' using ($1-timemin):($3/100) title 'ccngetfile received / 100' with lines linewidth 2,\
# 'n1.face7.AandB100M.up' using ($1-timemin):($4 / 10000)  title 'face7 choice_delta' with lines linewidth 2,\
# 'n1.face9.AandB100M.up' using ($1-timemin):($4 / -10000) title 'face9 choice_delta' with lines linewidth 1;
 #'tcqueuemonitor.n2.log' using ($1-timemin):($2/tcqueuefactor) title 'n2 queue' with lines linewidth 2,\
 #'tcqueuemonitor.n3.log' using ($1-timemin):($2/tcqueuefactor) title 'n3 queue' with lines linewidth 2,\

 #'lifetimes' using ($1-timemin):2 title 'interest lifetimes' with points linewidth 1;
 #'n1.face7.AandB100M.up' using ($1-timemin):2 title 'AandB/100M face7 threshold' with lines linewidth 2,\
 #'n1.face9.AandB100M.up' using ($1-timemin):2 title 'AandB/100M face9 threshold' with xerrorbars linewidth 2,\
 #'thresholdup.face7' using ($1-timemin):(10.0):($1-timemin):($1-timemin+$2) title 'face 7 threshold up' with xerrorbars linewidth 2,\
 #'thresholdup.face9' using ($1-timemin):(10.0):($1-timemin):($1-timemin+$2) title 'face 9 threshold up' with xerrorbars linewidth 2,\

 #'n1.face7.AandB100M2.up' using ($1-timemin):2 title 'AandB/100M2 face7 threshold' with lines linewidth 2,\
 #'n1.face9.AandB100M2.up' using ($1-timemin):2 title 'AandB/100M2 face9 threshold' with lines linewidth 2,\
 #'ccngetfile2.win' using ($1-timemin):2 title 'ccngetfile windo 2' with lines linewidth 2,\
 
# 'n1.face7.AandB100M.up' using ($1-timemin):2 title 'AandB/100M face7 threshold' with lines linewidth 2,\
# 'n1.face9.AandB100M.up' using ($1-timemin):2 title 'AandB/100M face9 threshold' with lines linewidth 2,\

 #'n1.face7.AandB100M2.up' using ($1-timemin):3 title 'AandB/100M2 face7 pending interests' with lines linewidth 2,\
 #'n1.face9.AandB100M2.up' using ($1-timemin):3 title 'AandB/100M2 face9 pending interests' with lines linewidth 2,\


# 'n1.face7.onlyA.up' using ($1-timemin):3 title 'onlyA face7 pending interests' with lines linewidth 2,\
# 'n1.face7.AandB.up' using ($1-timemin):3 title 'AandB face7 pending interests' with lines linewidth 2,\
# 'n1.face7.onlyA.up' using ($1-timemin):2 title 'onlyA face7 threshold' with lines linewidth 2,\
# 'n1.face9.AandB.up' using ($1-timemin):3 title 'AandB face9 pending interests' with lines linewidth 2,\
# 'n1.face9.AandB.up' using ($1-timemin):2 title 'AandB face9 threshold' with lines linewidth 2,\
# 'n1.face7.AandB.up' using ($1-timemin):2 title 'AandB face7 threshold' with lines linewidth 2;

# 'ccngetfile2.win' using ($1-timemin):2 title 'ccngetfile window 2' with lines linewidth 2,\
# 'ccngetfile.win' using ($1-timemin):2 title 'ccngetfile window 1' with lines linewidth 2;

 #'n1.face9.onlyA.up' using ($1-timemin):2 title 'onlyA face9 threshold' with lines linewidth 2,\
 #'n1.face9.onlyA.up' using ($1-timemin):3 title 'onlyA face9 pending interests' with lines linewidth 2,\
