#!/bin/bash

CCND_LOG=n97.log
CCNGETFILE_LOG=ccngetfile.log

grep contentobjectt $CCND_LOG       | awk '{print $4 " " $1}' | rev | cut -c 4- | rev | sort > ccnd.int
grep contentobjectt $CCNGETFILE_LOG | awk '{print "ccnx:" $3 " " $2}' | rev | cut -c 3- | rev | sort > ccng.int
join --nocheck-order ccnd.int ccng.int > joined.int

