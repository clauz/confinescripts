#!/usr/bin/gnuplot

set terminal png notransparent truecolor large enhanced font DejaVuSansMono 10 size 1024,768;

set output 'out.png';
set ylabel 'values';
set xtics 5
set grid xtics ytics mytics;

set tmargin 1
set bmargin 2
set lmargin 5
set rmargin 1

#set xrange [13:30] 

plot \
 'joined.int' using ($3 - $2) title 'timediff' with points linewidth 1; 

