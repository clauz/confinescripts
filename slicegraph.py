#!/usr/bin/env python2

# Sliver connectivity graph
# assume traceroute is installed on all slivers

import json
import urllib2
import sys
import paramiko
import re
import socket
import threading
import networkx as nx
from common import ConfineSliver, fetchnparse, MAX_HOPS, simplifyGraph

if len(sys.argv) < 2:
    print "Usage: %s <slice number>" % sys.argv[0]
    sys.exit(1)

SLICE_NO = sys.argv[1]


# collect information
# sliver URIs from the given slice
jres = fetchnparse("http://controller.confine-project.eu/api/slices/" + str(SLICE_NO))
sliveruris = [ j['uri'] for j in jres["slivers"] ]

sliverGraph = nx.Graph()
myslivers = []

for sliveruri in sliveruris:
    sys.stderr.write('.')
    sys.stderr.flush()
    slivernumber = sliveruri.split('/')[-1]

    # get the IPv6 management address of the node that hosts the sliver
    nodeuri = fetchnparse(sliveruri)['node']['uri']
    nodeinfo = fetchnparse(nodeuri)
    if nodeinfo['set_state'] != "production":
        continue
    mgmtaddr = nodeinfo['mgmt_net']['addr']

    # fetch the node's state
    nodeatnodeuri = "http://[%s]/confine/api/node/" % mgmtaddr
    try:
        nodeatnodeinfo = fetchnparse(nodeatnodeuri)
    except urllib2.URLError:
        continue
    if nodeatnodeinfo['state'] != 'production':
        continue

    # get the sliver information from the node
    sliveratnodeuri = "http://[%s]/confine/api/slivers/%s" % (mgmtaddr, slivernumber)
    try:
        sliverinfo = fetchnparse(sliveratnodeuri)
    except urllib2.URLError:
        continue
    if sliverinfo['state'] != "started":
        continue

    cs = ConfineSliver(slivernumber, sliverGraph)
    cs.nodeinfo = nodeinfo
    cs.nodemgmtaddr = mgmtaddr
    cs.nodeatnodeinfo = nodeatnodeinfo
    cs.description = nodeatnodeinfo["description"]
    cs.sliverinfo = sliverinfo
    cs.performPing = False
    cs.performIperf = False
    cs.performTraceroute = True

    for interfaceinfo in sliverinfo['interfaces']:
        if interfaceinfo['type'] == "public4":
            cs.public4 = interfaceinfo['ipv4_addr']
        elif interfaceinfo['type'] == "management":
            cs.management = interfaceinfo['ipv6_addr']

    myslivers.append(cs)

sys.stderr.write("\n")

for s in myslivers:
    print "%s\t%s\t%s" % (s.number, s.management, s.public4)
print ""

# traceroute everybody from everybody

for src in myslivers:
    src.setDestinations(myslivers)
    src.start()

for src in myslivers:
    src.join()

#sliver graph
nx.write_dot(sliverGraph, 'out.dot')
sys.stdout.write("Graph output in out.dot\n")
sys.stderr.write("Graph output in out.dot\n")

simplifyGraph(sliverGraph)
nx.write_dot(sliverGraph, 'out_simple.dot')
sys.stdout.write("Graph output in out_simple.dot\n")
sys.stderr.write("Graph output in out_simple.dot\n")

