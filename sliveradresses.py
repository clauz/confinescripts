#!/usr/bin/env python2

# Print all sliver management addresses for a given slice

import json
import urllib2
import sys
import re
import socket
import threading
from common import fetchnparse

if len(sys.argv) < 2:
    print "Usage: %s <slice number>" % sys.argv[0]
    sys.exit(1)

SLICE_NO = sys.argv[1]


# collect information
# sliver URIs from the given slice
jres = fetchnparse("http://controller.confine-project.eu/api/slices/" + str(SLICE_NO))
sliveruris = [ j['uri'] for j in jres["slivers"] ]

myslivers = []

for sliveruri in sliveruris:
    sys.stderr.write('.')
    sys.stderr.flush()
    slivernumber = sliveruri.split('/')[-1]

    # get the IPv6 management address of the node that hosts the sliver
    nodeuri = fetchnparse(sliveruri)['node']['uri']
    nodeinfo = fetchnparse(nodeuri)
    if nodeinfo['set_state'] != "production":
        continue
    nodename = nodeinfo['name']
    # node management address
    mgmtaddr = nodeinfo['mgmt_net']['addr']

    # fetch the node's state
    nodeatnodeuri = "http://[%s]/confine/api/node/" % mgmtaddr
    try:
        nodeatnodeinfo = fetchnparse(nodeatnodeuri)
    except urllib2.URLError, e:
        sys.stderr.write(str(e))
        sys.stderr.flush()
        continue
    if nodeatnodeinfo['state'] != 'production':
        sys.stderr.write("no production")
        sys.stderr.flush()
        continue

    # get the sliver information from the node
    sliveratnodeuri = "http://[%s]/confine/api/slivers/%s" % (mgmtaddr, slivernumber)
    try:
        sliverinfo = fetchnparse(sliveratnodeuri)
    except urllib2.URLError, e:
        sys.stderr.write(str(e))
        sys.stderr.flush()
        continue
    if sliverinfo['state'] != "started":
        sys.stderr.write("not started")
        sys.stderr.flush()
        continue

    for interfaceinfo in sliverinfo['interfaces']:
        if interfaceinfo['type'] == 'management':
            print "%s %s" % (interfaceinfo['ipv6_addr'], nodename)

