#!/usr/bin/env python2

# Sliver connectivity matrix

import json
import urllib2
import sys
import paramiko
import re
import socket
import threading
from common import ConfineSliver, fetchnparse, MAX_HOPS

if len(sys.argv) < 2:
    print "Usage: %s <slice number>" % sys.argv[0]
    sys.exit(1)

SLICE_NO = sys.argv[1]


# collect information
# sliver URIs from the given slice
jres = fetchnparse("http://controller.confine-project.eu/api/slices/" + str(SLICE_NO))
sliveruris = [ j['uri'] for j in jres["slivers"] ]

myslivers = []

for sliveruri in sliveruris:
    sys.stderr.write('.')
    sys.stderr.flush()
    slivernumber = sliveruri.split('/')[-1]

    # get the IPv6 management address of the node that hosts the sliver
    nodeuri = fetchnparse(sliveruri)['node']['uri']
    nodeinfo = fetchnparse(nodeuri)
    if nodeinfo['set_state'] != "production":
        continue
    mgmtaddr = nodeinfo['mgmt_net']['addr']

    # fetch the node's state
    nodeatnodeuri = "http://[%s]/confine/api/node/" % mgmtaddr
    try:
        nodeatnodeinfo = fetchnparse(nodeatnodeuri)
    except urllib2.URLError, e:
        sys.stderr.write(str(e))
        sys.stderr.flush()
        continue
    if nodeatnodeinfo['state'] != 'production':
        sys.stderr.write("no production")
        sys.stderr.flush()
        continue

    # get the sliver information from the node
    sliveratnodeuri = "http://[%s]/confine/api/slivers/%s" % (mgmtaddr, slivernumber)
    try:
        sliverinfo = fetchnparse(sliveratnodeuri)
    except urllib2.URLError, e:
        sys.stderr.write(str(e))
        sys.stderr.flush()
        continue
    if sliverinfo['state'] != "started":
        sys.stderr.write("not started")
        sys.stderr.flush()
        continue

    cs = ConfineSliver(slivernumber)
    cs.nodeinfo = nodeinfo
    cs.nodemgmtaddr = mgmtaddr
    cs.nodeatnodeinfo = nodeatnodeinfo
    cs.description = nodeatnodeinfo["description"]
    cs.sliverinfo = sliverinfo
    cs.performIperf = False

    for interfaceinfo in sliverinfo['interfaces']:
        if interfaceinfo['type'] == "public4":
            cs.public4 = interfaceinfo['ipv4_addr']
        elif interfaceinfo['type'] == "management":
            cs.management = interfaceinfo['ipv6_addr']

    myslivers.append(cs)

sys.stderr.write("\n")

for s in myslivers:
    print "%s\t%s\t%s\t%s" % (s.number, s.management, s.public4, s.nodeinfo['name'])
print ""

# ping everybody from everybody

for src in myslivers:
    src.setDestinations(myslivers)
    src.start()

for src in myslivers:
    src.join()

# draw the connectivity matrix

print "\n"
# raw connectivity matrix
print "hops \t", "\t".join([s.number for s in myslivers])
for src in myslivers:
    print src.number, "\t", "\t".join([str(src.getHopsToDst(dst)) for dst in myslivers])
print "\n"

# do some analysis and filtering

# filter out hosts with no ssh connectivity
reachableslivers = [src for src in myslivers \
        if sum([src.getHopsToDst(dst) for dst in myslivers]) != MAX_HOPS * (len(myslivers)-1)]

# filter out hosts that are on the same node
repeatedslivers = [(src.number, dst.number) for src in reachableslivers for dst in reachableslivers \
        if src.number != dst.number and src.getHopsToDst(dst) == 0 and dst.getHopsToDst(src) == 0]

sys.stderr.write("repeatedslivers: " + str(repeatedslivers) + "\n")

samenodeslivers0 = []
for (x, y) in repeatedslivers:
    found = False
    for s in samenodeslivers0:
        if x in s:
            s.add(y)
            found = True
        elif y in s:
            s.add(x)
            found = True
    if not found:
        t = set()
        t.add(x)
        t.add(y)
        samenodeslivers0.append(t)

samenodeslivers = samenodeslivers0[:]
indexes = range(len(samenodeslivers))
# while all sets are not disjoint
while sum([len(samenodeslivers[i].intersection(samenodeslivers[j])) \
        for i in indexes for j in indexes if i != j ]) > 0:
    mergedslivers = set()
    res = []
    for i in indexes:
        s = samenodeslivers[i]
        for j in indexes[s:]:
            t = samenodeslivers[j]
            if i != j and len(s.intersection(t)) > 1:
                    res.append(s.union(t))
                    mergedslivers.add(i)
                    mergedslivers.add(j)
        if not i in mergedslivers:
            res.append(s)
    samenodeslivers = res[:]


sys.stderr.write("samenodeslivers: " + str(samenodeslivers) + "\n")

discardedslivers = []
for s in samenodeslivers:
    assert len(s) > 1
    keptelement = list(s)[0]
    discardelements = s - set([keptelement])
    discardedslivers.extend(list(discardelements))

sys.stderr.write("discardedslivers: " + str(discardedslivers) + "\n")
sys.stdout.write("discardedslivers: " + str(discardedslivers) + "\n\n")

goodslivers = [sliver for sliver in reachableslivers if not sliver.number in discardedslivers]
    
def markedhops(src, dst):
    sh = str(src.getHopsToDst(dst))
    dh = str(dst.getHopsToDst(src))
    if sh == "256":
        return "INF"
    elif sh != dh:
        return sh + "~"
    elif sh == dh and sh != "0":
        return sh + "="
    else:
        return sh

print "hops \t", "\t".join([s.nodeinfo['name'] for s in goodslivers])
for src in goodslivers:
    print src.nodeinfo['name'], "\t", "\t".join([markedhops(src, dst) for dst in goodslivers])

print ""

print "rtt \t", "\t".join([s.nodeinfo['name'] for s in goodslivers])
for src in goodslivers:
    print src.nodeinfo['name'], "\t", "\t".join(["%.2f" % src.getRtt(dst) for dst in goodslivers])

