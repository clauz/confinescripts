#!/usr/bin/env python2

import json
import urllib2
import sys

if len(sys.argv) < 2:
    print "Usage: %s <slice number>" % (sys.argv[0],)
    sys.exit(1)

SLICE_NO = sys.argv[1]

class ConfineSliver():
    def __init__(self, slivernumber=-1):
        self.number = slivernumber
        self.public4 = None
        self.management = None

def fetchnparse(uri):
    f = urllib2.urlopen(uri)
    res = f.read()
    f.close()
    jres = json.loads(res)
    return jres

# sliver URIs from the given slice
jres = fetchnparse("http://controller.confine-project.eu/api/slices/" + str(SLICE_NO))
sliveruris = [ j['uri'] for j in jres["slivers"] ]

myslivers = []


for sliveruri in sliveruris:
    sys.stderr.write('.')
    sys.stderr.flush()
    slivernumber = sliveruri.split('/')[-1]

    # get the IPv6 management address of the node that hosts the sliver
    nodeuri = fetchnparse(sliveruri)['node']['uri']
    nodeinfo = fetchnparse(nodeuri)
    if nodeinfo['set_state'] != "production":
        continue
    mgmtaddr = nodeinfo['mgmt_net']['addr']

    # fetch the node's state
    nodeatnodeuri = "http://[%s]/confine/api/node/" % (mgmtaddr,)
    try:
        nodeatnodeinfo = fetchnparse(nodeatnodeuri)
    except urllib2.URLError:
        continue
    if nodeatnodeinfo['state'] != 'production':
        continue

    # get the sliver information from the node
    sliveratnodeuri = "http://[%s]/confine/api/slivers/%s" % (mgmtaddr, slivernumber)
    try:
        sliverinfo = fetchnparse(sliveratnodeuri)
    except urllib2.URLError:
        continue
    if sliverinfo['state'] != "started":
        continue

    cs = ConfineSliver(slivernumber)
    cs.nodeinfo = nodeinfo
    cs.nodemgmtaddr = mgmtaddr
    cs.nodeatnodeinfo = nodeatnodeinfo
    cs.description = nodeatnodeinfo["description"]
    cs.sliverinfo = sliverinfo

    for interfaceinfo in sliverinfo['interfaces']:
        if interfaceinfo['type'] == "public4":
            cs.public4 = interfaceinfo['ipv4_addr']
        elif interfaceinfo['type'] == "management":
            cs.management = interfaceinfo['ipv6_addr']

    myslivers.append(cs)

sys.stderr.write("\n")

for sliver in myslivers:
    print sliver.number, sliver.management, sliver.public4

