
addresses="fdf5:5351:1dfd:0097:1002:0000:0000:006d fdf5:5351:1dfd:008c:1002:0000:0000:006d fdf5:5351:1dfd:008b:1002:0000:0000:006d fdf5:5351:1dfd:0083:1002:0000:0000:006d fdf5:5351:1dfd:0081:1002:0000:0000:006d fdf5:5351:1dfd:007c:1002:0000:0000:006d fdf5:5351:1dfd:0078:1002:0000:0000:006d fdf5:5351:1dfd:006f:1002:0000:0000:006d fdf5:5351:1dfd:0063:1002:0000:0000:006d fdf5:5351:1dfd:0062:1002:0000:0000:006d fdf5:5351:1dfd:005a:1002:0000:0000:006d fdf5:5351:1dfd:0011:1002:0000:0000:006d fdf5:5351:1dfd:0003:1002:0000:0000:006d fdf5:5351:1dfd:0002:1002:0000:0000:006d fdf5:5351:1dfd:0001:1002:0000:0000:006d"


#!/bin/bash

for target in $addresses; do
    echo ">" $target
    scp -o "UserKnownHostsFile /dev/null" -o "StrictHostKeyChecking no" iperf_2.0.4-5_i386.deb root@[$target]:
    ssh -o "UserKnownHostsFile /dev/null" -o "StrictHostKeyChecking no" -l root $target "dpkg -i iperf_2.0.4-5_i386.deb"
done
